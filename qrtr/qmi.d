module qrtr.qmi;

import core.thread;
import std.format;
import std.stdio;

import qrtr.libqrtr;
import qrtr.qrtr;

alias qmi_message_type QMIMessageType;
alias qmi_result_v01 QMIResultV01;
alias qmi_elem_info QMIElementInfo;
alias qmi_elem_type QMIElementType;
alias qmi_array_type QMIArrayType;

private immutable ulong defaultBufferSize = 4096;

struct QMIService
{
	uint service;
	ushort ver;
	ushort instance;
};

struct QMIHeader
{
	align (1):
		ubyte type;
		ushort txnId;
		ushort msgId;
		ushort msgLen;
};

struct QMITransaction(T)
{
	uint id;
	T message;
};
struct QMITransaction(T: void)
{
	uint id;
	immutable void[] message = null;
};

QMIHeader QMIDecodeHeader(QRTRPacket packet)
{
	if (packet.data_len == 0)
		throw new Exception("Cannot decode header from empty packet");

	return *(cast(QMIHeader *)packet.data);
}

QMITransaction!T QMIDecodeMessage(T)(
	QRTRPacket packet,
	QMIMessageType type,
	uint msgId,
	QMIElementInfo[] elementInfos
) {
	QMITransaction!T txn;
	int ret;

	ret = qmi_decode_message(
		&txn.message,
		&txn.id,
		&packet,
		type,
		msgId,
		elementInfos.ptr
	);
	if (ret < 0)
		throw new Exception("Failed to decode message");

	return txn;
}
QMITransaction!T QMIDecodeMessage(T)(
	QRTRPacket packet,
	QMIMessageType type,
	uint msgId,
	immutable QMIElementInfo[] elementInfos
) {
	return QMIDecodeMessage!T(
		packet,
		type,
		msgId,
		cast(QMIElementInfo[])elementInfos
	);
}

QRTRPacket QMIEncodeMessage(T)(
	QMIMessageType type,
	uint msgId,
	QMITransaction!T txn,
	immutable QMIElementInfo[] elementInfos
) {
	QRTRPacket packet;
	long ret;

	packet.data = new ubyte[defaultBufferSize].ptr;

	if (is(T == void))
		/* Empty message */
		packet.data_len = QMIHeader.sizeof;
	else
		packet.data_len = defaultBufferSize;

	ret = qmi_encode_message(
		&packet,
		type,
		msgId,
		txn.id,
		&txn.message,
		cast(QMIElementInfo *)elementInfos.ptr
	);
	if (ret < 0)
		throw new Exception("Failed to encode message");

	return packet;
}

private interface QMIActor
{
	nothrow void registerCtrlHandler(QRTRPacketType type, void delegate(QRTRPacket) handler);
	nothrow void registerDataHandler(uint msgId, void delegate(QRTRPacket) handler);

	void wait();
	void runner();
	void process(uint timeout);

	ushort generateTxnId();
}

private class QMIBaseActor: QMIActor
{
	QRTR qrtr;
	immutable QMIService service;

	private Thread thread;
	uint timeout;	

	private void delegate(QRTRPacket)[uint] ctrlHandlers;
	private void delegate(QRTRPacket)[uint] dataHandlers;

	private ushort txnId;

	this(QMIService service, uint timeout)
	{
		this.service = service;
		this.timeout = timeout;

		this.thread = new Thread(delegate void()
		{
			this.qrtr = new QRTR(this.service.service);
			scope(exit) this.qrtr.bye(this.service);

			this.runner();
		});
	}

	final nothrow void registerCtrlHandler(
		QRTRPacketType type,
		void delegate(QRTRPacket) handler
	) {
		this.ctrlHandlers[type] = handler;
	}

	final nothrow void registerDataHandler(
		uint msgId,
		void delegate(QRTRPacket) handler
	) {
		this.dataHandlers[msgId] = handler;
	}

	void start()
	{
		this.thread.start();
	}

	void wait()
	{
		this.thread.join();
	}

	void runner() { }

	void process(uint timeout)
	{
		int pollfds;

		QRTRPacket packet;

		pollfds = this.qrtr.poll(timeout);
		if (!pollfds)
			return;

		packet = this.qrtr.receive();

		if (packet.type == QRTRPacketType.QRTR_TYPE_DATA) {
			/* Handle data packets */
			QMIHeader header = QMIDecodeHeader(packet);

			/* Update transaction ID counter */
			if (header.txnId > this.txnId)
				this.txnId = cast(ushort)(header.txnId + 1);

			if (header.msgId in this.dataHandlers)
				this.dataHandlers[header.msgId](packet);
		} else {
			/* Handle control packets */
			if (packet.type in this.ctrlHandlers)
				this.ctrlHandlers[packet.type](packet);
		}
	}

	final @safe nothrow ushort generateTxnId()
	{
		return this.txnId++;
	}
}

class QMIServer: QMIBaseActor
{
	this(QMIService service, uint timeout) { super(service, timeout); }

	override void runner()
	{
		this.qrtr.publish(this.service);

		while(true) {
			try
				process(this.timeout);
			catch (Exception e) {
				writeln(e);
				break;
			}
		}
	}
}

class QMIClient: QMIBaseActor
{
	this(QMIService service, uint timeout) { super(service, timeout); }

	override void runner()
	{
		this.qrtr.newLookup(this.service);

		while(true) {
			try
				process(this.timeout);
			catch (Exception e) {
				writeln(e);
				break;
			}
		}
	}
}
