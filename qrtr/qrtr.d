module qrtr.qrtr;

import std.stdio;

import qrtr.libqrtr;
import qrtr.qmi; 

alias qrtr_packet QRTRPacket;
alias qrtr_pkt_type QRTRPacketType;

private immutable ulong defaultBufferSize = 4096;

struct QRTRMessage
{
	uint node;
	uint port;
	ubyte[] data;
};

QRTRPacket QRTRDecode(ubyte[] buf)
{
	int ret;
	QRTRPacket packet;
	sockaddr_qrtr sq;

	ret = qrtr_decode(&packet, buf.ptr, cast(uint)buf.length, &sq);
	if (ret)
		throw new Exception("Failed to decode");

	return packet;
}

final class QRTR
{
	private immutable int sock;

	this(int rport = 0)
	{
		this.sock = qrtr_open(rport);
		if (sock < 0)
			throw new Exception("Failed to open QRTR port");
	}

	~this()
	{
		qrtr_close(this.sock);
	}

	void send(QRTRPacket packet)
	{
		int ret;
		
		ret = qrtr_sendto(
			this.sock,
			packet.node,
			packet.port,
			packet.data,
			cast(uint)packet.data_len
		);
		if (ret)
			throw new Exception("Failed to send data");
	}

	QRTRPacket receive()
	{
		QRTRPacket packet;
		sockaddr_qrtr sq;
		ubyte[] buffer = new ubyte[](defaultBufferSize);
		int ret;

		ret = qrtr_recvfrom(
			this.sock,
			buffer.ptr,
			cast(uint)buffer.length,
			&sq.sq_node,
			&sq.sq_port
		);
		if (ret < 0)
			throw new Exception("Failed to receive data");

		buffer.length = ret;

		ret = qrtr_decode(&packet, buffer.ptr, buffer.length, &sq);
		if (ret < 0)
			throw new Exception("Failed to decode data");

		return packet;
	}

	void recv(void[] data)
	{
		int ret;
		
		ret = qrtr_recv(this.sock, data.ptr, cast(uint)data.length);
		if (ret < 0)
			throw new Exception("Failed to receive data");

		data.length = ret;
	}

	int poll(uint ms)
	{
		int ret;

		ret = qrtr_poll(this.sock, ms);
		if (ret < 0)
			throw new Exception("Failed to poll");
		
		return ret;
	}

	void newServer(QMIService service)
	{
		int ret;

		ret = qrtr_new_server(this.sock, service.service, service.ver, service.instance);
		if (ret)
			throw new Exception("Failed to create server");
	}

	void removeServer(QMIService service)
	{
		int ret;

		ret = qrtr_remove_server(this.sock, service.service, service.ver, service.instance);
		if (ret)
			throw new Exception("Failed to remove server");
	}

	void publish(QMIService service)
	{
		int ret;

		ret = qrtr_publish(this.sock, service.service, service.ver, service.instance);
		if (ret)
			throw new Exception("Failed to publish service");
	}

	void bye(QMIService service)
	{
		int ret;

		ret = qrtr_bye(this.sock, service.service, service.ver, service.instance);
		if (ret)
			throw new Exception("Failed to say bye");
	}

	void newLookup(QMIService service)
	{
		int ret;

		ret = qrtr_new_lookup(this.sock, service.service, service.ver, service.instance);
		if (ret)
			throw new Exception("Failed to create lookup");
	}

	void removeLookup(QMIService service)
	{
		int ret;

		ret = qrtr_remove_lookup(this.sock, service.service, service.ver, service.instance);
		if (ret)
			throw new Exception("Failed to remove lookup");
	}
}
