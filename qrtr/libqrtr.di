module qrtr.libqrtr;

import core.sys.posix.sys.types;

extern (C) {
	/* sockaddr_qrtr definition in Linux */
	struct sockaddr_qrtr
	{
		ushort sq_family;
		uint sq_node;
		uint sq_port;
	};

	struct qrtr_packet
	{
		int type;

		uint node;
		uint port;

		uint service;
		uint instance;
		uint version_;

		void* data;
		size_t data_len;
	}


	enum qrtr_pkt_type {
		QRTR_TYPE_DATA		= 1,
		QRTR_TYPE_HELLO		= 2,
		QRTR_TYPE_BYE		= 3,
		QRTR_TYPE_NEW_SERVER	= 4,
		QRTR_TYPE_DEL_SERVER	= 5,
		QRTR_TYPE_DEL_CLIENT	= 6,
		QRTR_TYPE_RESUME_TX	= 7,
		QRTR_TYPE_EXIT		= 8,
		QRTR_TYPE_PING		= 9,
		QRTR_TYPE_NEW_LOOKUP	= 10,
		QRTR_TYPE_DEL_LOOKUP	= 11
	};

	enum qmi_message_type {
		REQUEST			= 0,
		RESPONSE		= 2,
		INDICATION		= 4
	};

	enum qmi_result_v01 {
		SUCCESS			= 0,
		FAILURE			= 1
	};

	enum qmi_error_v01 {
		NONE			= 0,
		MALFORMED_MS		= 1,
		NO_MEMORY		= 2,
		INTERNAL		= 3,
		CLIENT_IDS_EXHAUSTED	= 5,
		INVALID_ID		= 41,
		ENCODING		= 58,
		INCOMPATIBLE_STATE	= 90,
		NOT_SUPPORTED		= 94
	}

	enum qmi_elem_type
	{
		QMI_EOTI		= 0,
		QMI_OPT_FLAG		= 1,
		QMI_DATA_LEN		= 2,
		QMI_UNSIGNED_1_BYTE	= 3,
		QMI_UNSIGNED_2_BYTE	= 4,
		QMI_UNSIGNED_4_BYTE	= 5,
		QMI_UNSIGNED_8_BYTE	= 6,
		QMI_SIGNED_1_BYTE_ENUM	= 7,
		QMI_SIGNED_2_BYTE_ENUM	= 8,
		QMI_SIGNED_4_BYTE_ENUM	= 9,
		QMI_STRUCT		= 10,
		QMI_STRING		= 11
	}

	enum qmi_array_type
	{
		NO_ARRAY		= 0,
		STATIC_ARRAY		= 1,
		VAR_LEN_ARRAY		= 2
	}

	/**
	* struct qmi_elem_info - describes how to encode a single QMI element
	* @data_type:  Data type of this element.
	* @elem_len:   Array length of this element, if an array.
	* @elem_size:  Size of a single instance of this data type.
	* @array_type: Array type of this element.
	* @tlv_type:   QMI message specific type to identify which element
	*              is present in an incoming message.
	* @offset:     Specifies the offset of the first instance of this
	*              element in the data structure.
	* @ei_array:   Null-terminated array of @qmi_elem_info to describe nested
	*              structures.
	*/
	struct qmi_elem_info
	{
		qmi_elem_type data_type;
		uint elem_len;
		uint elem_size;
		qmi_array_type array_type;
		ubyte tlv_type;
		size_t offset;
		immutable qmi_elem_info *ei_array;
	}

	/**
	* qmi_response_type_v01 - common response header (decoded)
	* @result:     result of the transaction
	* @error:      error value, when @result is QMI_RESULT_FAILURE_V01
	*/
	struct qmi_response_type_v01
	{
		ushort result;
		ushort error;
	}

	extern __gshared qmi_elem_info[] qmi_response_type_v01_ei;

	int qrtr_open (int rport);
	void qrtr_close (int sock);

	int qrtr_sendto (int sock, uint node, uint port, const(void)* data, uint sz);
	int qrtr_recvfrom (int sock, void* buf, uint bsz, uint* node, uint* port);
	int qrtr_recv (int sock, void* buf, uint bsz);

	int qrtr_new_server (int sock, uint service, ushort version_, ushort instance);
	int qrtr_remove_server (int sock, uint service, ushort version_, ushort instance);

	int qrtr_publish (int sock, uint service, ushort version_, ushort instance);
	int qrtr_bye (int sock, uint service, ushort version_, ushort instance);

	int qrtr_new_lookup (int sock, uint service, ushort version_, ushort instance);
	int qrtr_remove_lookup (int sock, uint service, ushort version_, ushort instance);

	int qrtr_poll (int sock, uint ms);

	int qrtr_decode (
		qrtr_packet* dest,
		void* buf,
		size_t len,
		const(sockaddr_qrtr)* sq);

	int qmi_decode_header (const(qrtr_packet)* pkt, uint* msg_id);
	int qmi_decode_message (
		void* c_struct,
		uint* txn,
		const(qrtr_packet)* pkt,
		int type,
		int id,
		qmi_elem_info* ei);
	long qmi_encode_message (
		qrtr_packet* pkt,
		int type,
		int msg_id,
		int txn_id,
		const(void)* c_struct,
		qmi_elem_info* ei);
};
