module frontend;

import core.thread;
import std.concurrency;
import std.format;
import std.stdio;

import backend;
import frontend.sensor;

class Frontend
{
	Backend[uint] backends;
	private Sensor[] sensors;

	Thread thread;
	Tid tid;

	this(Backend[] backends)
	{
		this.thread = new Thread(&this.process);

		foreach (Backend backend; backends) {
			backend.frontend = this;
			this.backends[backend.uid] = backend;
		}

		this.thread.start();
	}

	private void onNewSensor(Backend backend, uint id)
	{
		Sensor sensor = new Sensor(backend, id);

		writeln("Frontend: Found %s %s @%02x".format(sensor.vendor, sensor.name, id));

		this.sensors ~= sensor;
	}

	private void process() {
		this.tid = thisTid;

		foreach (Backend backend; this.backends) {
			backend.start();
		}

		while (true) {
			try
				receive(
					(immutable NewSensor msg) {
						this.onNewSensor(
							this.backends[msg.backendUid],
							msg.sensorId
						);
					}
				);
			catch (Exception e)
				writeln(e);
				break;
		}
	}
}
