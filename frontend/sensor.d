module frontend.sensor;

import backend;

enum SensorType
{
	UNKNOWN,
	ACCELEROMETER,
	GYROSCOPE,
	MAGNETOMETER,
	PROXIMITY,
	LIGHT,
	PRESSURE,
	TEMPERATURE,
	HALL_EFFECT
};

interface SensorInterface
{
	@property string name();
	@property string vendor();

	@property SensorType type();

	@property bool enabled();
	@property void enabled(bool);
}

class Sensor: SensorInterface
{
	Backend backend;
	immutable uint backendId;

	this(Backend backend, uint backendId)
	{
		this.backend = backend;
		this.backendId = backendId;
	}

	override @property string name()
	{
		return backend.getSensorName(backendId);
	}

	override @property string vendor()
	{
		return backend.getSensorVendor(backendId);
	}

	override @property SensorType type()
	{
		return backend.getSensorType(backendId);
	}

	override @property bool enabled()
	{
		return backend.isSensorEnabled(backendId);
	}

	override @property void enabled(bool enable)
	{
		return backend.enableSensor(backendId, enable);
	}
}

/* TODO: Make a class for each type of sensor */
