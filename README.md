# SensorManager
System service for managing smart sensor hubs

## Supported (work-in-progress) sensor hubs:
- Qualcomm SSC V1 (pure QMI, sensor manager service 0x100)
---
## Dependencies
- [libqrtr](https://github.com/andersson/qrtr)
## Building

SCons along with a D compiler are required. Only dmd has been tested so far.
```
$ scons
```
## Running
### Qualcomm SSC V1
A `sns.reg` file suitable for the device must be placed in the work directory.
