module backend;

import core.thread;

import frontend;
import frontend.sensor;

import backend.ssc_v1;

interface BackendInterface
{
	@property Frontend frontend();
	@property void frontend(Frontend);

	void start();

	string getSensorName(uint);
	string getSensorVendor(uint);

	SensorType getSensorType(uint);

	bool isSensorEnabled(uint);
	void enableSensor(uint, bool);
}

class Backend: BackendInterface
{
	immutable uint uid;

	private Frontend fe;

	@property Frontend frontend() { return this.fe; }
	@property void frontend(Frontend frontend) { this.fe = frontend; }

	void start() { }

	string getSensorName(uint) { return ""; }
	string getSensorVendor(uint) { return ""; }

	SensorType getSensorType(uint) { return SensorType.UNKNOWN; }

	bool isSensorEnabled(uint) { return false; }
	void enableSensor(uint, bool) { }
}

struct NewSensor
{
	uint backendUid;
	uint sensorId;
}

@property Backend[] backends()
{
	return [
		new SSCv1Backend(0)
	];
}
