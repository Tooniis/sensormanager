module backend.ssc_v1;

import std.concurrency;
import std.format;
import std.stdio;

import backend;
import backend.ssc_v1.mgr;
import backend.ssc_v1.reg;
import frontend;
import frontend.sensor;

final class SSCv1Backend: Backend
{
	private SensorRegistryServer sensorRegistryServer;
	private SensorManagerClient sensorManagerClient;

	private Frontend fe;

	immutable uint uid;

	this(uint uid)
	{
		this.uid = uid;

		this.sensorRegistryServer = new SensorRegistryServer(uint.max);
		this.sensorManagerClient = new SensorManagerClient(uint.max);

		this.sensorManagerClient.newSensorCallback = &this.onNewSensor;
	}

	override @property Frontend frontend() { return this.fe; }
	override @property void frontend(Frontend frontend)
	{
		this.fe = frontend;

		/* Report all available sensors to new frontend */
		foreach (uint key; sensorManagerClient.sensors.keys)
			send!(immutable NewSensor)(
				frontend.tid,
				immutable NewSensor(this.uid, key)
			);
	}

	override void start()
	{
		this.sensorRegistryServer.start();
		this.sensorManagerClient.start();
	}

	void onNewSensor(uint id)
	{
		send!(immutable NewSensor)(this.frontend.tid, immutable NewSensor(this.uid, id));
	}

	override string getSensorName(uint id)
	{
		SensorManagerSensor sensor = this.sensorManagerClient.sensors[id];

		/* TODO: Support multiple channels */
		return sensor.channels[0].name;
	}

	override string getSensorVendor(uint id)
	{
		SensorManagerSensor sensor = this.sensorManagerClient.sensors[id];

		/* TODO: Support multiple channels */
		return sensor.channels[0].vendor;
	}

	override SensorType getSensorType(uint id)
	{
		/* TODO: Implement */
		return SensorType.UNKNOWN;
	}

	override bool isSensorEnabled(uint id)
	{
		/* TODO: Implement */
		return false;
	}

	override void enableSensor(uint id, bool enable)
	{
		/* TODO: Implement */
	}
}
