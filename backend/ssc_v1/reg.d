module backend.ssc_v1.reg;

import std.format;
import std.functional;
import std.stdio;

import qrtr.qmi;
import qrtr.qrtr;

immutable QMIService SensorRegistryService = {
	service: 0x010f,
	ver: 2,
	instance: 0
};

enum SensorRegistryMessage
{
	KEY = 4
};

/* Message types */

struct SensorRegistryKeyRequest
{
	ushort key;
};

immutable QMIElementInfo[2] SensorRegistryKeyRequestEI = [
	{
		data_type: QMIElementType.QMI_UNSIGNED_2_BYTE,
		elem_len: 1,
		elem_size: ushort.sizeof,
		array_type: QMIArrayType.NO_ARRAY,
		tlv_type: 0x01,
		offset: SensorRegistryKeyRequest.key.offsetof
	},
	{ data_type: QMIElementType.QMI_EOTI }
];

immutable uint sensorRegistryEntryMaxLength = 0x100;

struct SensorRegistryEntryValue
{
	ulong length;
	ubyte[sensorRegistryEntryMaxLength] data;
};

immutable QMIElementInfo[3] SensorRegistryEntryValueEI = [
	{
		data_type: QMIElementType.QMI_DATA_LEN,
		elem_len: 1,
		elem_size: ulong.sizeof,
		array_type: QMIArrayType.NO_ARRAY,
		offset: SensorRegistryEntryValue.length.offsetof
	},
	{
		data_type: QMIElementType.QMI_UNSIGNED_1_BYTE,
		elem_len: sensorRegistryEntryMaxLength,
		elem_size: ubyte.sizeof,
		array_type: QMIArrayType.VAR_LEN_ARRAY,
		offset: SensorRegistryEntryValue.data.offsetof
	},
	{ data_type: QMIElementType.QMI_EOTI }
];

struct SensorRegistryKeyResponse
{
	ushort result;
	ushort key;
	SensorRegistryEntryValue value;
};

immutable QMIElementInfo[4] SensorRegistryKeyResponseEI = [
	{
		data_type: QMIElementType.QMI_UNSIGNED_2_BYTE,
		elem_len: 1,
		elem_size: ushort.sizeof,
		array_type: QMIArrayType.NO_ARRAY,
		tlv_type: 0x02,
		offset: SensorRegistryKeyResponse.result.offsetof
	},
	{
		data_type: QMIElementType.QMI_UNSIGNED_2_BYTE,
		elem_len: 1,
		elem_size: ushort.sizeof,
		array_type: QMIArrayType.NO_ARRAY,
		tlv_type: 0x03,
		offset: SensorRegistryKeyResponse.key.offsetof
	},
	{
		data_type: QMIElementType.QMI_STRUCT,
		elem_len: 1,
		elem_size: SensorRegistryEntryValue.sizeof,
		array_type: QMIArrayType.NO_ARRAY,
		tlv_type: 0x04,
		offset: SensorRegistryKeyResponse.value.offsetof,
		ei_array: SensorRegistryEntryValueEI.ptr
	},
	{ data_type: QMIElementType.QMI_EOTI }
];

/* Registry map */

immutable struct SensorRegistryEntry
{
	ushort address;
	ulong length;
};

private immutable SensorRegistryEntry[uint] SensorRegistryMap;
shared static this()
{
	SensorRegistryMap = [
		0x000: SensorRegistryEntry(0x0000, 0x018),
		0x00a: SensorRegistryEntry(0x0800, 0x018),
		0x3e8: SensorRegistryEntry(0x0a00, 0x003),
		0x3f2: SensorRegistryEntry(0x0c00, 0x003),
		0x3fc: SensorRegistryEntry(0x0d00, 0x003),
		0x410: SensorRegistryEntry(0x0100, 0x080),
		0x7d0: SensorRegistryEntry(0x0200, 0x010),
		0x7d2: SensorRegistryEntry(0x0400, 0x018),
		0x802: SensorRegistryEntry(0x1100, 0x00c),
		0xa3c: SensorRegistryEntry(0x0e00, 0x024),
		0xa46: SensorRegistryEntry(0x0f00, 0x018),
		0xa50: SensorRegistryEntry(0x1000, 0x00a),
		0xa6e: SensorRegistryEntry(0x1500, 0x010),
		0xa82: SensorRegistryEntry(0x1700, 0x100),
		0xa84: SensorRegistryEntry(0x1800, 0x100),
		0xa85: SensorRegistryEntry(0x1900, 0x100),
		0xa86: SensorRegistryEntry(0x1a00, 0x100),
		0xa87: SensorRegistryEntry(0x1b00, 0x100),
		0xa88: SensorRegistryEntry(0x1c00, 0x100),
		0xa8a: SensorRegistryEntry(0x2700, 0x100),
		0xa8b: SensorRegistryEntry(0x2d00, 0x100),
		0xa8c: SensorRegistryEntry(0x1d00, 0x0e0),
		0xaf0: SensorRegistryEntry(0x1f00, 0x022),
		0xb54: SensorRegistryEntry(0x2000, 0x004),
		0xb5e: SensorRegistryEntry(0x2100, 0x004),
		0xb68: SensorRegistryEntry(0x2200, 0x004),
		0xb72: SensorRegistryEntry(0x2300, 0x004),
		0xb7c: SensorRegistryEntry(0x2400, 0x024),
		0xb86: SensorRegistryEntry(0x2500, 0x008),
		0xb90: SensorRegistryEntry(0x2800, 0x004),
		0xbb8: SensorRegistryEntry(0x2e00, 0x100),
		0xbc2: SensorRegistryEntry(0x3100, 0x100),
		0xbcc: SensorRegistryEntry(0x3500, 0x100),
		0xbe0: SensorRegistryEntry(0x3a00, 0x014),
		0xbfe: SensorRegistryEntry(0x3c00, 0x00c),
		0xc08: SensorRegistryEntry(0x3f00, 0x05a),
		0xc12: SensorRegistryEntry(0x6000, 0x014),
		0xce4: SensorRegistryEntry(0x4200, 0x00e),
		0xce5: SensorRegistryEntry(0x4300, 0x00e),
		0xce6: SensorRegistryEntry(0x4400, 0x00e),
		0xce7: SensorRegistryEntry(0x4500, 0x00e),
		0xce8: SensorRegistryEntry(0x4600, 0x00e),
		0xce9: SensorRegistryEntry(0x4700, 0x00e),
		0xcea: SensorRegistryEntry(0x4800, 0x00e),
		0xceb: SensorRegistryEntry(0x4900, 0x00e),
		0xcec: SensorRegistryEntry(0x4a00, 0x00e),
		0xced: SensorRegistryEntry(0x4b00, 0x00e),
		0xcee: SensorRegistryEntry(0x4c00, 0x00e),
		0xcef: SensorRegistryEntry(0x4d00, 0x00e),
		0xcf0: SensorRegistryEntry(0x4e00, 0x00e),
		0xcf1: SensorRegistryEntry(0x4f00, 0x00e),
		0xcf2: SensorRegistryEntry(0x5000, 0x00e),
		0xcf3: SensorRegistryEntry(0x5100, 0x00e),
		0xcf4: SensorRegistryEntry(0x5200, 0x00e),
		0xcf5: SensorRegistryEntry(0x5300, 0x00e),
		0xcf6: SensorRegistryEntry(0x5400, 0x00e),
		0xcf7: SensorRegistryEntry(0x5500, 0x00e),
		0xcf8: SensorRegistryEntry(0x5600, 0x00e),
		0xcf9: SensorRegistryEntry(0x5700, 0x00e),
		0xcfa: SensorRegistryEntry(0x5800, 0x00e),
		0xcfb: SensorRegistryEntry(0x5900, 0x00e),
		0xcfc: SensorRegistryEntry(0x5a00, 0x00e),
		0xcfd: SensorRegistryEntry(0x5b00, 0x00e),
		0xcfe: SensorRegistryEntry(0x5c00, 0x00e),
		0xcff: SensorRegistryEntry(0x5d00, 0x00e),
		0xd00: SensorRegistryEntry(0x5e00, 0x00e),
		0xd01: SensorRegistryEntry(0x5f00, 0x00e),
		0xd48: SensorRegistryEntry(0x6100, 0x01c)
	];
}

class SensorRegistryServer: QMIServer
{
	File registry;

	this(uint timeout)
	{
		registry.open("sns.reg", "rb");

		super(SensorRegistryService, timeout);

		super.registerDataHandler(
			SensorRegistryMessage.KEY,
			toDelegate(&this.handleKeyRequest)
		);
	}

	private void handleKeyRequest(QRTRPacket inPacket)
	{
		QMITransaction!SensorRegistryKeyRequest requestTxn;
		QMITransaction!SensorRegistryKeyResponse responseTxn;
		QRTRPacket outPacket;

		requestTxn = QMIDecodeMessage!SensorRegistryKeyRequest(
			inPacket,
			QMIMessageType.REQUEST,
			SensorRegistryMessage.KEY,
			SensorRegistryKeyRequestEI
		);

		writeln("Requested key 0x%04x".format(requestTxn.message.key));

		responseTxn.id = requestTxn.id;
		responseTxn.message.key = requestTxn.message.key;

		if (requestTxn.message.key !in SensorRegistryMap) {
			writeln("Key unmapped!");

			responseTxn.message.result = QMIResultV01.FAILURE;
		} else {
			SensorRegistryEntry entry = SensorRegistryMap[requestTxn.message.key];

			responseTxn.message.result = QMIResultV01.SUCCESS;
			responseTxn.message.value.length = entry.length;

			/* Read entry value from registry */
			this.registry.seek(entry.address);
			ubyte[] value = this.registry.rawRead(new ubyte[entry.length]);

			/* Copy value to packet buffer */
			for (int i = 0; i < entry.length; ++i)
				responseTxn.message.value.data[i] = value[i];
		}

		outPacket = QMIEncodeMessage!SensorRegistryKeyResponse(
			QMIMessageType.RESPONSE,
			SensorRegistryMessage.KEY,
			responseTxn,
			SensorRegistryKeyResponseEI
		);
		outPacket.node = inPacket.node;
		outPacket.port = inPacket.port;

		this.qrtr.send(outPacket);
	};
}
