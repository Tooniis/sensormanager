module backend.ssc_v1.mgr;

import std.format;
import std.functional;
import std.stdio;
import std.string;

import qrtr.qmi;
import qrtr.qrtr;

private:

immutable QMIService SensorManagerService = {
	service: 0x0100,
	ver: 1,
	instance: 50
};

enum SensorManagerMessage
{
	QUERY_SENSORS		= 0x5,
	QUERY_SENSOR_DETAILS	= 0x6,
	ENABLE_SENSOR		= 0x21,
	SENSOR_DATA		= 0x22
};

immutable uint sensorManagerTypeMaxLength = 0x0f;
immutable uint sensorManagerSensorMaxCount = 0x0f;

struct SensorManagerQuerySensorsInfo
{
	ubyte id;
	ubyte typeLength;
	char[sensorManagerTypeMaxLength] type;
};

immutable QMIElementInfo[4] sensorManagerQuerySensorsInfoEI = [
	{
		data_type: QMIElementType.QMI_UNSIGNED_1_BYTE,
		elem_len: 1,
		elem_size: ubyte.sizeof,
		array_type: QMIArrayType.NO_ARRAY,
		offset: SensorManagerQuerySensorsInfo.id.offsetof
	},
	{
		data_type: QMIElementType.QMI_DATA_LEN,
		elem_len: 1,
		elem_size: ubyte.sizeof,
		array_type: QMIArrayType.NO_ARRAY,
		offset: SensorManagerQuerySensorsInfo.typeLength.offsetof
	},
	{
		data_type: QMIElementType.QMI_UNSIGNED_1_BYTE,
		elem_len: sensorManagerTypeMaxLength,
		elem_size: ubyte.sizeof,
		array_type: QMIArrayType.VAR_LEN_ARRAY,
		offset: SensorManagerQuerySensorsInfo.type.offsetof
	},
	{ data_type: QMIElementType.QMI_EOTI }
];

struct SensorManagerQuerySensorsList
{
	ubyte count;
	SensorManagerQuerySensorsInfo[sensorManagerSensorMaxCount] infos;
};

immutable QMIElementInfo[3] sensorManagerQuerySensorsListEI = [
	{
		data_type: QMIElementType.QMI_DATA_LEN,
		elem_len: 1,
		elem_size: ubyte.sizeof,
		array_type: QMIArrayType.NO_ARRAY,
		offset: SensorManagerQuerySensorsList.count.offsetof
	},
	{
		data_type: QMIElementType.QMI_STRUCT,
		elem_len: sensorManagerSensorMaxCount,
		elem_size: SensorManagerQuerySensorsInfo.sizeof,
		array_type: QMIArrayType.VAR_LEN_ARRAY,
		offset: SensorManagerQuerySensorsList.infos.offsetof,
		ei_array: sensorManagerQuerySensorsInfoEI.ptr
	},
	{ data_type: QMIElementType.QMI_EOTI }
];

struct SensorManagerQuerySensorsResponse
{
	ushort result;
	SensorManagerQuerySensorsList list;
};

immutable QMIElementInfo[3] sensorManagerQuerySensorsResponseEI = [
	{
		data_type: QMIElementType.QMI_UNSIGNED_2_BYTE,
		elem_len: 1,
		elem_size: ushort.sizeof,
		array_type: QMIArrayType.NO_ARRAY,
		tlv_type: 0x02,
		offset: SensorManagerQuerySensorsResponse.result.offsetof
	},
	{
		data_type: QMIElementType.QMI_STRUCT,
		elem_len: 1,
		elem_size: SensorManagerQuerySensorsList.sizeof,
		array_type: QMIArrayType.NO_ARRAY,
		tlv_type: 0x03,
		offset: SensorManagerQuerySensorsResponse.list.offsetof,
		ei_array: sensorManagerQuerySensorsListEI.ptr
	},
	{ data_type: QMIElementType.QMI_EOTI }
];

struct SensorManagerQuerySensorDetailsRequest
{
	ubyte id;
}

immutable QMIElementInfo[2] sensorManagerQuerySensorDetailsRequestEI = [
	{
		data_type: QMIElementType.QMI_UNSIGNED_1_BYTE,
		elem_len: 1,
		elem_size: ubyte.sizeof,
		array_type: QMIArrayType.NO_ARRAY,
		tlv_type: 0x01,
		offset: SensorManagerQuerySensorDetailsRequest.id.offsetof
	},
	{ data_type: QMIElementType.QMI_EOTI }
];

immutable ulong sensorDetailsNameMaxLength = 0xff;
immutable ulong sensorDetailsChannelUnknown1MaxLength = 18;

struct SensorManagerQuerySensorDetailsChannel
{
	ubyte sensorId;
	ubyte channelId;
	ubyte nameLen;
	char[sensorDetailsNameMaxLength] name;
	ubyte vendorLen;
	char[sensorDetailsNameMaxLength] vendor;
	ubyte[sensorDetailsChannelUnknown1MaxLength] unknown1;
};

immutable QMIElementInfo[8] sensorManagerQuerySensorDetailsChannelEI = [
	{
		data_type: QMIElementType.QMI_UNSIGNED_1_BYTE,
		elem_len: 1,
		elem_size: ubyte.sizeof,
		array_type: QMIArrayType.NO_ARRAY,
		offset: SensorManagerQuerySensorDetailsChannel.sensorId.offsetof
	},
	{
		data_type: QMIElementType.QMI_UNSIGNED_1_BYTE,
		elem_len: 1,
		elem_size: ubyte.sizeof,
		array_type: QMIArrayType.NO_ARRAY,
		offset: SensorManagerQuerySensorDetailsChannel.channelId.offsetof
	},
	{
		data_type: QMIElementType.QMI_DATA_LEN,
		elem_len: 1,
		elem_size: ubyte.sizeof,
		array_type: QMIArrayType.NO_ARRAY,
		offset: SensorManagerQuerySensorDetailsChannel.nameLen.offsetof
	},
	{
		data_type: QMIElementType.QMI_UNSIGNED_1_BYTE,
		elem_len: sensorDetailsNameMaxLength,
		elem_size: ubyte.sizeof,
		array_type: QMIArrayType.VAR_LEN_ARRAY,
		offset: SensorManagerQuerySensorDetailsChannel.name.offsetof
	},
	{
		data_type: QMIElementType.QMI_DATA_LEN,
		elem_len: 1,
		elem_size: ubyte.sizeof,
		array_type: QMIArrayType.NO_ARRAY,
		offset: SensorManagerQuerySensorDetailsChannel.vendorLen.offsetof
	},
	{
		data_type: QMIElementType.QMI_UNSIGNED_1_BYTE,
		elem_len: sensorDetailsNameMaxLength,
		elem_size: ubyte.sizeof,
		array_type: QMIArrayType.VAR_LEN_ARRAY,
		offset: SensorManagerQuerySensorDetailsChannel.vendor.offsetof
	},
	{
		data_type: QMIElementType.QMI_UNSIGNED_1_BYTE,
		elem_len: sensorDetailsChannelUnknown1MaxLength,
		elem_size: ubyte.sizeof,
		array_type: QMIArrayType.STATIC_ARRAY,
		offset: SensorManagerQuerySensorDetailsChannel.unknown1.offsetof
	},
	{ data_type: QMIElementType.QMI_EOTI }
];

immutable ulong sensorDetailsChannelsMaxCount = 2;

struct SensorManagerQuerySensorDetails
{
	ubyte channelCount;
	SensorManagerQuerySensorDetailsChannel[sensorDetailsChannelsMaxCount] channels;
};

immutable QMIElementInfo[3] sensorManagerQuerySensorDetailsEI = [
	{
		data_type: QMIElementType.QMI_DATA_LEN,
		elem_len: 1,
		elem_size: ubyte.sizeof,
		array_type: QMIArrayType.NO_ARRAY,
		offset: SensorManagerQuerySensorDetails.channelCount.offsetof
	},
	{
		data_type: QMIElementType.QMI_STRUCT,
		elem_len: sensorDetailsChannelsMaxCount,
		elem_size: SensorManagerQuerySensorDetailsChannel.sizeof,
		array_type: QMIArrayType.VAR_LEN_ARRAY,
		offset: SensorManagerQuerySensorDetails.channels.offsetof,
		ei_array: sensorManagerQuerySensorDetailsChannelEI.ptr
	},
	{ data_type: QMIElementType.QMI_EOTI }
];

immutable ulong sensorDetailsUnknown1MaxLength = 9;
immutable ulong sensorDetailsUnknown2MaxLength = 17;
immutable ulong sensorDetailsUnknown3MaxLength = 22;
immutable ulong sensorDetailsUnknown4MaxLength = 9;

struct SensorManagerQuerySensorDetailsResponse
{
	ushort result;
	SensorManagerQuerySensorDetails details; 
	ubyte[sensorDetailsUnknown1MaxLength] unknown1;
	uint bus;
	ubyte[sensorDetailsUnknown2MaxLength] unknown2;
	ubyte unknown3_valid;
	ubyte[sensorDetailsUnknown3MaxLength] unknown3;
	ubyte[sensorDetailsUnknown4MaxLength] unknown4;
};

QMIElementInfo[9] sensorManagerQuerySensorDetailsResponseEI = [
	{
		data_type: QMIElementType.QMI_UNSIGNED_2_BYTE,
		elem_len: 1,
		elem_size: ushort.sizeof,
		array_type: QMIArrayType.NO_ARRAY,
		tlv_type: 0x02,
		offset: SensorManagerQuerySensorDetailsResponse.result.offsetof
	},
	{
		data_type: QMIElementType.QMI_STRUCT,
		elem_len: 1,
		elem_size: SensorManagerQuerySensorDetails.sizeof,
		array_type: QMIArrayType.NO_ARRAY,
		tlv_type: 0x03,
		offset: SensorManagerQuerySensorDetailsResponse.details.offsetof,
		ei_array: sensorManagerQuerySensorDetailsEI.ptr
	},
	{
		data_type: QMIElementType.QMI_UNSIGNED_1_BYTE,
		/* elem_len is dynamically set according to sensor type */
		elem_size: ubyte.sizeof,
		array_type: QMIArrayType.STATIC_ARRAY,
		tlv_type: 0x10,
		offset: SensorManagerQuerySensorDetailsResponse.unknown1.offsetof
	},
	{
		data_type: QMIElementType.QMI_UNSIGNED_4_BYTE,
		elem_len: 1,
		elem_size: uint.sizeof,
		array_type: QMIArrayType.NO_ARRAY,
		tlv_type: 0x11,
		offset: SensorManagerQuerySensorDetailsResponse.bus.offsetof
	},
	{
		data_type: QMIElementType.QMI_UNSIGNED_1_BYTE,
		/* elem_len is dynamically set according to sensor type */
		elem_size: ubyte.sizeof,
		array_type: QMIArrayType.STATIC_ARRAY,
		tlv_type: 0x12,
		offset: SensorManagerQuerySensorDetailsResponse.unknown2.offsetof
	},
	{
		data_type: QMIElementType.QMI_OPT_FLAG,
		elem_len: 1,
		elem_size: ubyte.sizeof,
		array_type: QMIArrayType.NO_ARRAY,
		tlv_type: 0x13,
		offset: SensorManagerQuerySensorDetailsResponse.unknown3_valid.offsetof
	},
	{
		data_type: QMIElementType.QMI_UNSIGNED_1_BYTE,
		/* elem_len is dynamically set according to sensor type */
		elem_size: ubyte.sizeof,
		array_type: QMIArrayType.STATIC_ARRAY,
		tlv_type: 0x13,
		offset: SensorManagerQuerySensorDetailsResponse.unknown3.offsetof
	},
	{
		data_type: QMIElementType.QMI_UNSIGNED_1_BYTE,
		/* elem_len is dynamically set according to sensor type */
		elem_size: ubyte.sizeof,
		array_type: QMIArrayType.STATIC_ARRAY,
		tlv_type: 0x14,
		offset: SensorManagerQuerySensorDetailsResponse.unknown4.offsetof
	},
	{ data_type: QMIElementType.QMI_EOTI }
];

public enum SensorManagerSensorType
{
	ACCEL,
	GYRO,
	MAG,
	PROX_LIGHT,
	PRESSURE,
	HALL_EFFECT
};

immutable SensorManagerSensorType[string] sensorManagerSensorTypes;
shared static this() {
	sensorManagerSensorTypes = [
		"ACCEL":	SensorManagerSensorType.ACCEL,
		"GYRO":		SensorManagerSensorType.GYRO,
		"MAG":		SensorManagerSensorType.MAG,
		"PROX_LIGHT":	SensorManagerSensorType.PROX_LIGHT,
		"PRESSURE":	SensorManagerSensorType.PRESSURE,
		"HALL_EFFECT":	SensorManagerSensorType.HALL_EFFECT
	];
}

immutable uint[4][SensorManagerSensorType.max + 1] sensorManagerSensorTypeUnknownLengths = [
	SensorManagerSensorType.ACCEL:		[0x05, 0x09, 0x16, 0x05],
	SensorManagerSensorType.GYRO:		[0x05, 0x09, 0x16, 0x05],
	SensorManagerSensorType.MAG:		[0x05, 0x09, 0x0a, 0x05],
	SensorManagerSensorType.PROX_LIGHT:	[0x09, 0x11, 0x07, 0x09],
	SensorManagerSensorType.PRESSURE:	[0x09, 0x11, 0x00, 0x09],
	SensorManagerSensorType.HALL_EFFECT:	[0x05, 0x09, 0x04, 0x05],
];

struct SensorManagerSensorChannel
{
	string vendor;
	string name;
}

public struct SensorManagerSensor
{
	SensorManagerSensorType type;
	SensorManagerSensorChannel[] channels = [];
}

public class SensorManagerClient: QMIClient
{
	private uint serverNode;
	private uint serverPort;

	private uint[uint] querySensorDetailsTxns;

	SensorManagerSensor[uint] sensors;

	void delegate(uint) newSensorCallback;

	this(uint timeout) {
		super(SensorManagerService, timeout);

		super.registerCtrlHandler(
			QRTRPacketType.QRTR_TYPE_NEW_SERVER,
			toDelegate(&this.handleNewServer)
		);

		super.registerDataHandler(
			SensorManagerMessage.QUERY_SENSORS,
			toDelegate(&this.handleQuerySensors)
		);

		super.registerDataHandler(
			SensorManagerMessage.QUERY_SENSOR_DETAILS,
			toDelegate(&this.handleQuerySensorDetails)
		);
	}

	private void handleNewServer(QRTRPacket inPacket)
	{
		QRTRPacket outPacket;
		QMITransaction!void requestTxn;

		/* Remember server node and port for future transactions */
		this.serverNode = inPacket.node;
		this.serverPort = inPacket.port;
		
		requestTxn.id = this.generateTxnId();

		/* Query sensors */
		outPacket = QMIEncodeMessage!void(
			QMIMessageType.REQUEST,
			SensorManagerMessage.QUERY_SENSORS,
			requestTxn,
			null
		);
		outPacket.node = this.serverNode;
		outPacket.port = this.serverPort;

		this.qrtr.send(outPacket);
	}

	private void handleQuerySensors(QRTRPacket inPacket) {
		QMITransaction!SensorManagerQuerySensorsResponse responseTxn;

		responseTxn = QMIDecodeMessage!SensorManagerQuerySensorsResponse(
			inPacket,
			QMIMessageType.RESPONSE,
			SensorManagerMessage.QUERY_SENSORS,
			sensorManagerQuerySensorsResponseEI
		);

		for (uint i = 0; i < responseTxn.message.list.count; ++i) {
			SensorManagerQuerySensorsInfo info = responseTxn.message.list.infos[i];
			string type = cast(string)info.type[0..info.typeLength];

			writeln("Found sensor: %s@%02x".format(type, info.id));

			this.sensors[info.id] = SensorManagerSensor();
			this.sensors[info.id].type =
				sensorManagerSensorTypes[type];

			this.querySensorDetails(info.id);
		}
	}

	private void querySensorDetails(ubyte id)
	{
		QMITransaction!SensorManagerQuerySensorDetailsRequest requestTxn;
		QRTRPacket outPacket;

		requestTxn.id = this.generateTxnId();
		requestTxn.message.id = id;

		outPacket = QMIEncodeMessage!SensorManagerQuerySensorDetailsRequest(
			QMIMessageType.REQUEST,
			SensorManagerMessage.QUERY_SENSOR_DETAILS,
			requestTxn,
			sensorManagerQuerySensorDetailsRequestEI
		);
		outPacket.node = this.serverNode;
		outPacket.port = this.serverPort;

		this.qrtr.send(outPacket);
		/*
		 * Remember transaction ID of query. Will be needed later
		 * to identify the response
		 */
		querySensorDetailsTxns[requestTxn.id] = id;
	}

	private void handleQuerySensorDetails(QRTRPacket inPacket)
	{
		QMIHeader responseHeader;
		QMITransaction!SensorManagerQuerySensorDetailsResponse responseTxn;

		responseHeader = QMIDecodeHeader(inPacket);

		SensorManagerSensorType sensorType = this.sensors[
			this.querySensorDetailsTxns[responseHeader.txnId]
		].type;

		/* TODO: Rework this */
		/* Set unknown1 length */
		sensorManagerQuerySensorDetailsResponseEI[2].elem_len =
			sensorManagerSensorTypeUnknownLengths[sensorType][0];
		/* Set unknown2 length */
		sensorManagerQuerySensorDetailsResponseEI[4].elem_len =
			sensorManagerSensorTypeUnknownLengths[sensorType][1];
		/* Set unknown3 length */
		sensorManagerQuerySensorDetailsResponseEI[6].elem_len =
			sensorManagerSensorTypeUnknownLengths[sensorType][2];
		/* Set unknown4 length */
		sensorManagerQuerySensorDetailsResponseEI[7].elem_len =
			sensorManagerSensorTypeUnknownLengths[sensorType][3];

		responseTxn = QMIDecodeMessage!SensorManagerQuerySensorDetailsResponse(
			inPacket,
			QMIMessageType.RESPONSE,
			SensorManagerMessage.QUERY_SENSOR_DETAILS,
			sensorManagerQuerySensorDetailsResponseEI
		);

		for (uint i = 0; i < responseTxn.message.details.channelCount; ++i) {
			SensorManagerQuerySensorDetailsChannel queryChannel =
				responseTxn.message.details.channels[i];

			SensorManagerSensorChannel channel = {
				name: cast(string)queryChannel.name[0..queryChannel.nameLen],
				vendor: cast(string)queryChannel.vendor[0..queryChannel.vendorLen],
			};

			writeln(
				"%02x: %s %s".format(
					queryChannel.sensorId,
					channel.vendor,
					channel.name
				)
			);

			this.sensors[
				this.querySensorDetailsTxns[responseHeader.txnId]
			].channels ~= channel;
		}

		/* This transaction ID will no longer be needed. Remove it. */
		this.querySensorDetailsTxns.remove(responseHeader.txnId);

		/* TODO: Support multiple channels. Use first channel only for now. */
		this.newSensorCallback(responseTxn.message.details.channels[0].sensorId);
	}
}
