import std.concurrency;
import std.stdio;

import backend;
import frontend;

int main(string[] args)
{
	Frontend frontend = new Frontend(backends);

	frontend.thread.join();

	return 0;
}
